package com.example.erick.proyectomovil;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddSaleActivity extends AppCompatActivity
{
	private static final String TAG = AddSaleActivity.class.getName();
	private static final String URL = "product/from/";
	private static final String URL_ADD_SALE = "sale/add/";
	private static final String URL_ADD_SALE_DETAIL = "sale/detail/add/";

	ListView lvAddSaleItem;
	AddSaleAdapter adapter;
	List<AddSale> arrayOfSaleItems;

	Button btAddItem;
	FloatingActionButton fab;

	ArrayList<AddSale> addSales;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_sale);

		lvAddSaleItem = (ListView) findViewById(R.id.lvAddSaleItem);
		arrayOfSaleItems = new ArrayList<>();
		arrayOfSaleItems.add(new AddSale());
		adapter = new AddSaleAdapter(this, arrayOfSaleItems);
		lvAddSaleItem.setAdapter(adapter);

		btAddItem = (Button) findViewById(R.id.btAddItem);
		btAddItem.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				adapter.add(new AddSale());
				adapter.notifyDataSetChanged();
			}
		});

		fab = (FloatingActionButton) findViewById(R.id.fabAddSale);
		fab.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				addSales = new ArrayList<>();
				for (int i = 0; i < adapter.getCount(); i++) {
					AddSale addSale = adapter.getItem(i);
					if (addSale == null) {
						Log.e(TAG, "child null");
						Toast.makeText(AddSaleActivity.this,
								getResources().getString(R.string.error_try_later),
								Toast.LENGTH_SHORT).show();
						return;
					}
					String productCode = addSale.productCode;
					int qty = addSale.qty;
					int pricelvl = addSale.priceLevel;
					int price = addSale.price;
					if (productCode.equals("") || qty == 0 || pricelvl == 0) {
						Toast.makeText(AddSaleActivity.this,
								getResources().getString(R.string.error_invalid_info_add_sale),
								Toast.LENGTH_SHORT).show();
						return;
					}
					Client client = addSale.clientObject;
					addSales.add(new AddSale(productCode, qty, client.id, client,
							pricelvl, price));
				}
				loadProducts();
			}
		});
	}

	private void loadProducts()
	{
		long id;
		try {
			id = MainActivity.dManager.getLastProductId();
		} catch (Exception e) {
			id = 0;
			Log.e(TAG, e.getMessage());
		}
		String url = MainActivity.URL + URL + Long.toString(id);
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						onProductsLoad(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.w(TAG, error.getMessage());
					}
				}){
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	private void onProductsLoad(String response)
	{
		try {
			JSONObject obj = new JSONObject(response);
			JSONArray productsArray = obj.getJSONArray("products");
			for (int i = 0; i < productsArray.length(); i++) {
				JSONObject productObject = productsArray.getJSONObject(i);
				Product product = new Product(productObject.getInt("id"),
						productObject.getString("code"), productObject.getString("name"),
						productObject.getInt("price"), productObject.getInt("price1"),
						productObject.getInt("price2"), productObject.getInt("price3"),
						productObject.getInt("price4"), productObject.getInt("price5"),
						productObject.getInt("price6"));
				MainActivity.dManager.addProduct(product);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		createSale(0);
	}

	private void createSale(int index)
	{
		if (index == -1) {
			finish();
			return;
		}
		int currentClient = -1;
		ArrayList<SaleDetail> saleDetails = new ArrayList<>();
		Sale sale;
		AddSale addSale;
		int subtotal = 0;
		long id;
		Product product;
		Client client = null;
		for (; index < addSales.size(); index++) {
			addSale = addSales.get(index);
			if (currentClient == -1) {
				currentClient = addSales.get(index).client;
				client = addSale.clientObject;
			} else if (currentClient != addSales.get(index).client) {
				currentClient = addSale.client;
				sale = new Sale(-1, client.id, client.name, null,
						subtotal, subtotal);
				id = MainActivity.dManager.addSale(sale);

				if (id == -1) continue;
				for (int j = 0; j < saleDetails.size(); j++) {
					saleDetails.get(j).sale = (int) id;
					MainActivity.dManager.addSaleDetail(saleDetails.get(j));
				}
				addSaleToServer(sale, saleDetails, index);
				return;
			}
			product = MainActivity.dManager.getProductByCode(addSale.productCode.toLowerCase());
			int price = addSale.price * addSale.qty;
			subtotal += price;
			saleDetails.add(new SaleDetail(-1, -1, product.id, product.name, addSale.qty, price));
		}

		if (client != null) {
			sale = new Sale(-1, client.id, client.name, null,
					subtotal, subtotal);
			id = MainActivity.dManager.addSale(sale);
			if (id == -1) return;
			for (int j = 0; j < saleDetails.size(); j++) {
				saleDetails.get(j).sale = (int) id;
				MainActivity.dManager.addSaleDetail(saleDetails.get(j));
			}
			addSaleToServer(sale, saleDetails, -1);
			return;
		}
	}

	private void addSaleToServer(final Sale sale, final ArrayList<SaleDetail> saleDetails,
								 final int index)
	{
		String url = MainActivity.URL + URL_ADD_SALE;
		StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
				new Response.Listener<String>()
				{
					@Override
					public void onResponse(String response)
					{
						try {
							JSONObject obj = new JSONObject(response);
							if (obj.getBoolean("added")) {
								addSaleDetailsToServer(saleDetails, index, 0);
							} else {
								MainActivity.dManager.deleteSale(sale.id);
								try {
									String error = obj.getString("error");
									Toast.makeText(AddSaleActivity.this, error,
											Toast.LENGTH_SHORT).show();
								} catch (Exception e) {
									Toast.makeText(AddSaleActivity.this,
											getResources().getString(R.string.error_try_later),
											Toast.LENGTH_SHORT).show();
								}
								Log.e(TAG, "Couldn't create sale");
							}
						} catch (Exception e) {
							Log.w("Some", e.getMessage());
							MainActivity.dManager.deleteSale(sale.id);
						}
					}
				},
				new Response.ErrorListener()
				{
					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(AddSaleActivity.this, error.toString(),
								Toast.LENGTH_LONG).show();
						MainActivity.dManager.deleteSale(sale.id);
					}
				}){
			@Override
			protected Map<String, String> getParams()
			{
				Map<String,String> params = new HashMap<String, String>();
				if (sale.id != -1) {
					params.put("id", Integer.toString(sale.id));
				}
				params.put("client", Integer.toString(sale.client));
				params.put("date", (sale.date == null) ? "" : sale.date);
				params.put("subtotal", Integer.toString(sale.subtotal));
				params.put("total", Integer.toString(sale.total));
				return params;
			}

		};
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
		Log.d(TAG, "Adding request");
		requestQueue.add(stringRequest);
	}

	private void addSaleDetailsToServer(final ArrayList<SaleDetail> saleDetails,
										final int index, final int arrayIndex)
	{
		String url = MainActivity.URL + URL_ADD_SALE_DETAIL;
		StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						try {
							JSONObject obj = new JSONObject(response);
							if (obj.getBoolean("added")) {
								if (arrayIndex == saleDetails.size() - 1) {
									if (index == -1) finish();
									createSale(index);
								} else {
									addSaleDetailsToServer(saleDetails, index, arrayIndex + 1);
								}
							} else {
								try {
									MainActivity.dManager.deleteSale(saleDetails
											.get(arrayIndex).sale);
									String error = obj.getString("error");
									Toast.makeText(AddSaleActivity.this, error,
											Toast.LENGTH_SHORT).show();
								} catch (Exception e) {
									Toast.makeText(AddSaleActivity.this,
											getResources().getString(R.string.error_try_later),
											Toast.LENGTH_SHORT).show();
								}
								Log.e(TAG, "Couldn't create Sale Detail");
							}
						} catch (Exception e) {
							Log.w("Some", e.getMessage());
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(AddSaleActivity.this, error.toString(),
								Toast.LENGTH_LONG).show();
					}
				}){
			@Override
			protected Map<String,String> getParams(){
				Map<String,String> params = new HashMap<String, String>();
				params.put("id", Integer.toString(saleDetails.get(arrayIndex).id));
				params.put("sale", Integer.toString(saleDetails.get(arrayIndex).sale));
				params.put("product", Integer.toString(saleDetails.get(arrayIndex).product));
				params.put("qty", Integer.toString(saleDetails.get(arrayIndex).qty));
				params.put("subtotal", Integer.toString(saleDetails.get(arrayIndex).subtotal));
				return params;
			}

		};
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
		Log.d(TAG, "Adding request");
		requestQueue.add(stringRequest);
	}
}
