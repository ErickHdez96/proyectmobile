package com.example.erick.proyectomovil;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

class ClientsAdapter extends ArrayAdapter<Client>
{
    private static class ViewHolder
	{
        TextView name;
        TextView email;
    }

    ClientsAdapter(Context context, List<Client> objects)
	{
        super(context, R.layout.item_client, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		Client client = getItem(position);
		ViewHolder viewHolder; // view lookup cache stored in tag
		if (convertView == null) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.item_client, parent, false);
			viewHolder.name = (TextView) convertView.findViewById(R.id.tvClientName);
			viewHolder.email = (TextView) convertView.findViewById(R.id.tvClientEmail);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (client == null) {
            Log.e("CLIENT ADAPTER", "Client null");
        }
		viewHolder.name.setText(client.name);
		viewHolder.email.setText(client.email);
		return convertView;
	}
}
