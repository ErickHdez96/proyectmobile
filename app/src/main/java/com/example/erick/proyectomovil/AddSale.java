package com.example.erick.proyectomovil;

class AddSale
{
	String productCode;
	int qty;
	int client;
	Client clientObject;
	int priceLevel;
	int price;

	AddSale()
	{
		this.productCode = "";
		this.qty = 0;
		this.client = 0;
		this.priceLevel = 0;
		this.price = 0;
	}

	AddSale(String productCode, int qty, int client, Client clientObject, int priceLevel, int price)
	{
		this.productCode = productCode;
		this.qty = qty;
		this.client = client;
		this.clientObject = clientObject;
		this.priceLevel = priceLevel;
		this.price = price;
	}

	void addClientByName(String clientName)
	{
		this.clientObject = MainActivity.dManager.getClientByName(clientName);
		this.client = this.clientObject.id;
	}

	void addPrice(float price)
	{
		this.price = (int) (price * 100);
	}
}
