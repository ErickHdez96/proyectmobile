package com.example.erick.proyectomovil;

public class SaleDetail
{
	public int id;
	public int sale;
	public int product;
	public String productName;
	public int qty;
	public int subtotal;

	public SaleDetail(int id, int sale, int product, String productName, int qty, int subtotal)
	{
		this.id = id;
		this.sale = sale;
		this.product = product;
		this.productName = productName;
		this.qty = qty;
		this.subtotal = subtotal;
	}
}
