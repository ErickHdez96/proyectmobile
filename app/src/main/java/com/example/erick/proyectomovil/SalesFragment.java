package com.example.erick.proyectomovil;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalesFragment extends Fragment
{
	public SalesFragment()
	{
		// Required empty public constructor
	}

	private static final String TAG = SalesFragment.class.getName();
	private static final String URL = "sale/from/";

	ListView lvSales;
	ArrayList<Sale> arrayOfSales;
	SalesAdapter adapter;

	private View mProgressView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_sales, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
	{
		lvSales = (ListView) getActivity().findViewById(R.id.lvSales);
		arrayOfSales = new ArrayList<>();
		adapter = new SalesAdapter(getActivity(), arrayOfSales);
		lvSales.setAdapter(adapter);
		lvSales.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				Sale sale = adapter.getItem(i);
				if (sale == null) return;
				Intent intent = new Intent(getActivity(), SaleDetailActivity.class);
				intent.putExtra("clientsName", sale.clientName);
				intent.putExtra("saleDate", sale.date);
				intent.putExtra("saleId", sale.id);
				startActivity(intent);
			}
		});

		mProgressView = getActivity().findViewById(R.id.pbSales);
		showProgress(true);

		loadCurrentSales();
		loadSales();
		super.onViewCreated(view, savedInstanceState);
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show)
	{
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			lvSales.setVisibility(show ? View.GONE : View.VISIBLE);
			lvSales.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					lvSales.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			lvSales.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	public void loadCurrentSales()
	{
		ArrayList<Sale> sales = MainActivity.dManager.getSales();
		for (Sale sale : sales) {
			arrayOfSales.add(sale);
		}
	}

	public void loadSales()
	{
		long id;
		try {
			id = MainActivity.dManager.getLastSaleId();
		} catch (Exception e) {
			id = 0;
			Log.e(TAG, e.getMessage());
		}
		String url = MainActivity.URL + URL + Long.toString(id);
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						onSalesLoad(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.w(TAG, error.getMessage());
					}
				}){
		};
		RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
		requestQueue.add(stringRequest);
	}

	public void onSalesLoad(String response)
	{
		try {
			JSONObject obj = new JSONObject(response);
			JSONArray salesArray = obj.getJSONArray("sales");
			for (int i = 0; i < salesArray.length(); i++) {
				JSONObject salesObject = salesArray.getJSONObject(i);
				Sale sale = new Sale(salesObject.getInt("id"), salesObject.getInt("client"),
						salesObject.getString("clientName"), salesObject.getString("date"),
						salesObject.getInt("subtotal"), salesObject.getInt("total"));
				adapter.add(sale);
				MainActivity.dManager.addSale(sale);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		showProgress(false);
	}

	public void addSale()
	{
		Intent intent = new Intent(getActivity(), AddSaleActivity.class);
		startActivity(intent);
	}
}
