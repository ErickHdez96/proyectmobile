package com.example.erick.proyectomovil;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

class AddSaleAdapter extends ArrayAdapter<AddSale>
{

	private static List<String> arrayOfClients;
	private static ArrayAdapter<String> adapter;

	private static class ViewHolder
	{
		EditText atvProduct;
		EditText etQty;
		Spinner spClients;
		EditText etPriceLevel;
		EditText etPrice;
	}

	AddSaleAdapter(Context context, List<AddSale> objects)
	{
		super(context, R.layout.item_add_sale, objects);

		if (arrayOfClients == null) {
			arrayOfClients = new ArrayList<>();
			ArrayList<Client> clients = MainActivity.dManager.getClients();
			for (int i = 0; i < clients.size(); i++) {
				arrayOfClients.add(clients.get(i).name);
			}
			adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item,
					arrayOfClients);
		}
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		final AddSale addSale = getItem(position);
		if (addSale == null) {
			return convertView;
		}
		final ViewHolder viewHolder; // view lookup cache stored in tag
		if (convertView == null) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.item_add_sale, parent, false);
			viewHolder.atvProduct = (EditText) convertView .findViewById(R.id.etAddSaleProduct);
			viewHolder.atvProduct.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(View view, boolean hasFocus)
				{
					if (!hasFocus) {
						addSale.productCode = viewHolder.atvProduct.getText().toString();
					}
				}
			});
			viewHolder.etQty = (EditText) convertView.findViewById(R.id.etAddSaleQty);
			viewHolder.etQty.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(View view, boolean b)
				{
					if (!b && !viewHolder.etQty.getText().toString().equals("")) {
						addSale.qty = Integer.parseInt(viewHolder.etQty.getText().toString());
					}
				}
			});
			viewHolder.spClients = (Spinner) convertView.findViewById(R.id.spAddSaleClient);
			viewHolder.spClients.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
			{
				@Override
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
				{
					addSale.addClientByName(adapterView.getSelectedItem().toString());
				}

				@Override
				public void onNothingSelected(AdapterView<?> adapterView)
				{
					int b = 0;
				}
			});
			viewHolder.etPriceLevel = (EditText) convertView.findViewById(R.id.etAddSalePriceLvl);
			viewHolder.etPriceLevel.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(View view, boolean b)
				{
					if (!b && !viewHolder.etPriceLevel.getText().toString().equals("")) {
						addSale.priceLevel = Integer.parseInt(viewHolder.etPriceLevel
								.getText().toString());
					}
				}
			});
			viewHolder.etPrice = (EditText) convertView.findViewById(R.id.etAddSalePrice);
			viewHolder.etPrice.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(View view, boolean b)
				{
					if (!b && !viewHolder.etPrice.getText().toString().equals("")) {
						addSale.addPrice(Float.parseFloat(viewHolder.etPrice
								.getText().toString()));
					}
				}
			});
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (viewHolder.spClients.getSelectedItem() == null){
			viewHolder.spClients.setAdapter(adapter);
		}
		return convertView;
	}
}
