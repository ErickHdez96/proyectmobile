package com.example.erick.proyectomovil;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class SaleDetailAdapter extends ArrayAdapter<SaleDetail>
{
	private static class ViewHolder
	{
		TextView tvProduct;
		TextView tvQty;
		TextView tvTotal;
	}

	SaleDetailAdapter(Context context, List<SaleDetail> objects)
	{
		super(context, R.layout.item_detail, objects);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		SaleDetail saleDetail = getItem(position);
		ViewHolder viewHolder; // view lookup cache stored in tag
		if (convertView == null) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.item_detail, parent, false);
			viewHolder.tvProduct = (TextView) convertView.findViewById(R.id.tvDetailProduct);
			viewHolder.tvQty = (TextView) convertView.findViewById(R.id.tvDetailQty);
			viewHolder.tvTotal = (TextView) convertView.findViewById(R.id.tvDetailTotal);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (saleDetail == null) {
			Log.e("SALEDETAIL ADAPTER", "Sale detail null");
			return convertView;
		}
		viewHolder.tvProduct.setText(saleDetail.productName);
		viewHolder.tvQty.setText(String.format(Locale.getDefault(), "%d", saleDetail.qty));
		viewHolder.tvTotal.setText("$" + Integer.toString(saleDetail.subtotal / 100) + "." +
				String.format(Locale.getDefault(), "%02d", saleDetail.subtotal % 100));
		return convertView;
	}
}
