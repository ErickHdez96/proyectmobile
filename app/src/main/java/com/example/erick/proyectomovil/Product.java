package com.example.erick.proyectomovil;

/**
 * Created by erick on 11/21/16.
 */

public class Product
{
	public int id;
	public String code;
	public String name;
	public int price;
	public int price1;
	public int price2;
	public int price3;
	public int price4;
	public int price5;
	public int price6;

	Product(int id, String code, String name, int price, int price1, int price2, int price3,
			int price4, int price5, int price6)
	{
		this.id = id;
		this.code = code;
		this.name = name;
		this.price = price;
		this.price1 = price1;
		this.price2 = price2;
		this.price3 = price3;
		this.price4 = price4;
		this.price5 = price5;
		this.price6 = price6;
	}

	Product(int id)
	{
		this.id = id;
		this.code = "";
		this.name = "";
		this.price = 0;
		this.price1 = 0;
		this.price2 = 0;
		this.price3 = 0;
		this.price4 = 0;
		this.price5 = 0;
		this.price6 = 0;
	}
}
