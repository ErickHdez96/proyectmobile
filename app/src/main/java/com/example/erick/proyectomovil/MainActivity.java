package com.example.erick.proyectomovil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.jar.Manifest;

public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener
{
	public static String PREFERENCES = "User_preferences";
	private static final String TAG = MainActivity.class.getName();
	private SharedPreferences prefs;
	public static final String URL = "http://192.168.1.65:8000/";
	public static DatabaseManager dManager;

	Toolbar toolbar;
	NavigationView navigationView;
	FloatingActionButton fab;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		prefs = getSharedPreferences(PREFERENCES, MODE_PRIVATE);
		String username = prefs.getString("username", "");
		if (username.equals("")) {
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			finish();
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		MainFragment fragment = new MainFragment();
		android.support.v4.app.FragmentTransaction fragmentTransaction =
				getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragment_container, fragment, "MAIN_FRAGMENT");
		fragmentTransaction.commit();

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ClientsFragment clientsFragment = (ClientsFragment) getSupportFragmentManager()
						.findFragmentByTag("CLIENTS_FRAGMENT");
				if (clientsFragment != null && clientsFragment.isVisible()) {
					clientsFragment.addClient();
				} else {
					MessagesFragment messagesFragment = (MessagesFragment)
							getSupportFragmentManager().findFragmentByTag("MESSAGES_FRAGMENT");
					if (messagesFragment != null && messagesFragment.isVisible()) {
						messagesFragment.loadMessages();
					} else {
						SalesFragment salesFragment = (SalesFragment) getSupportFragmentManager()
								.findFragmentByTag("SALES_FRAGMENT");
						if (salesFragment != null && salesFragment.isVisible()) {
							salesFragment.addSale();
						}
					}
				}
			}
		});
		fab.setVisibility(View.GONE);

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		dManager = new DatabaseManager(getApplicationContext());
	}

	@Override
	public void onBackPressed()
	{
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		if (!prefs.getString("username", "").equals("")) {
			TextView nav_username = (TextView) findViewById(R.id.nav_username);
			nav_username.setText(prefs.getString("username", ""));
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item)
	{
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.nav_home) {
			fab.setVisibility(View.GONE);
			MainFragment fragment = new MainFragment();
			android.support.v4.app.FragmentTransaction fragmentTransaction =
					getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fragment_container, fragment, "MAIN_FRAGMENT");
			fragmentTransaction.commit();
		} else if (id == R.id.nav_messages) {
			fab.setVisibility(View.VISIBLE);
			fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
					R.drawable.ic_refresh_black_24dp));
			MessagesFragment fragment = new MessagesFragment();
			android.support.v4.app.FragmentTransaction fragmentTransaction =
					getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fragment_container, fragment, "MESSAGES_FRAGMENT");
			fragmentTransaction.commit();
		} else if (id == R.id.nav_clients) {
			fab.setVisibility(View.VISIBLE);
			fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
					R.drawable.ic_add_circle_black_24dp));
			ClientsFragment fragment = new ClientsFragment();
			android.support.v4.app.FragmentTransaction fragmentTransaction =
					getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fragment_container, fragment, "CLIENTS_FRAGMENT");
			fragmentTransaction.commit();
		} else if (id == R.id.nav_sales) {
			fab.setVisibility(View.VISIBLE);
			fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
					R.drawable.ic_attach_money_black_24dp));
			SalesFragment fragment = new SalesFragment();
			android.support.v4.app.FragmentTransaction fragmentTransaction =
					getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fragment_container, fragment, "SALES_FRAGMENT");
			fragmentTransaction.commit();
		}

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

}
