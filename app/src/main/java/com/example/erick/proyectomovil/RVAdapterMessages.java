package com.example.erick.proyectomovil;

import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

class RVAdapterMessages extends RecyclerView.Adapter<RVAdapterMessages.MessageViewHolder>
{
    static class MessageViewHolder extends RecyclerView.ViewHolder
    {
        CardView cvMessage;
        TextView tvClient;
        TextView tvDate;
        TextView tvSubject;

        MessageViewHolder(View itemView)
        {
            super(itemView);
            cvMessage = (CardView) itemView.findViewById(R.id.cvMessage);
            tvClient = (TextView) itemView.findViewById(R.id.tvCardClientName);
            tvDate = (TextView) itemView.findViewById(R.id.tvCardMessageDate);
            tvSubject = (TextView) itemView.findViewById(R.id.tvCardMessageSubject);
        }
    }

    private ArrayList<Message> messages;

    RVAdapterMessages(ArrayList<Message> messages)
    {
        this.messages = messages;
    }

    @Override
    public int getItemCount()
    {
        return messages.size();
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new MessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position)
    {
        if (!messages.get(position).read) {
            holder.tvClient.setTypeface(null, Typeface.BOLD);
            holder.tvSubject.setTypeface(null, Typeface.BOLD);
        } else {
            holder.tvClient.setTypeface(null, Typeface.NORMAL);
            holder.tvSubject.setTypeface(null, Typeface.NORMAL);
        }
        holder.tvClient.setText(messages.get(position).clientName);
        holder.tvDate.setText(messages.get(position).date);
        holder.tvSubject.setText(messages.get(position).subject);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
