package com.example.erick.proyectomovil;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment
{
	private static final String TAG = "MESSAGES FRAGMENT";
	private static final String URL = "message/from/";

	RecyclerView rvMessages;
	LinearLayoutManager lmMessages;

	ArrayList<Message> arrayOfMessages;
	RVAdapterMessages adapter;

	private View mProgressView;

	public MessagesFragment()
	{
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_messages, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
	{
		arrayOfMessages = new ArrayList<>();
		adapter = new RVAdapterMessages(arrayOfMessages);

		lmMessages = new LinearLayoutManager(getActivity());
		rvMessages = (RecyclerView) getActivity().findViewById(R.id.rvMessages);
		rvMessages.setLayoutManager(lmMessages);
		rvMessages.setAdapter(adapter);

		mProgressView = getActivity().findViewById(R.id.pbMessages);

		showProgress(true);

		loadCurrentMessages();
		loadMessages();
		super.onViewCreated(view, savedInstanceState);
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show)
	{
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			rvMessages.setVisibility(show ? View.GONE : View.VISIBLE);
			rvMessages.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					rvMessages.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			rvMessages.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	public void loadCurrentMessages()
	{
		ArrayList<Message> messages = MainActivity.dManager.getMessages();
		for (Message message : messages) {
			arrayOfMessages.add(message);
		}
	}

	public void loadMessages()
	{
		long id;
		try {
			id = MainActivity.dManager.getLastMessageId();
		} catch (Exception e) {
			id = 0;
			Log.e(TAG, e.getMessage());
		}
		String url = MainActivity.URL + URL + Long.toString(id);
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						onMessagesLoad(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.w(TAG, error.getMessage());
					}
				}){
		};
		RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
		requestQueue.add(stringRequest);
	}

	private void onMessagesLoad(String response)
	{
		try {
			JSONObject obj = new JSONObject(response);
			JSONArray messageArray = obj.getJSONArray("messages");
			for (int i = 0; i < messageArray.length(); i++) {
				JSONObject messageObject = messageArray.getJSONObject(i);
				Message message = new Message(messageObject.getInt("id"),
						messageObject.getString("subject"), messageObject.getInt("client"),
						messageObject.getString("client_name"), messageObject.getString("body"),
						messageObject.getString("date"), messageObject.getBoolean("read"));
				arrayOfMessages.add(message);
				MainActivity.dManager.addMessage(message);
			}
			adapter.notifyDataSetChanged();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		showProgress(false);
	}
}
