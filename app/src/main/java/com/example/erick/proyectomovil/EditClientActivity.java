package com.example.erick.proyectomovil;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EditClientActivity extends AppCompatActivity
{
	private static final String URL_DELETE = "client/delete/";
	private static final String URL_EDIT = "client/edit/";
	private static final String TAG = EditClientActivity.class.getName();

	public static final int REQUEST_CODE = 0;

	EditText etName;
	EditText etEmail;
	EditText etPriceLevel;
	Button btEdit;
	Button btDelete;

	Client client;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_client);

		client = new Client(-1);

		etName = (EditText) findViewById(R.id.etClientName);
		etEmail = (EditText) findViewById(R.id.etClientEmail);
		etPriceLevel = (EditText) findViewById(R.id.etClientPriceLevel);

		btEdit = (Button) findViewById(R.id.btClientEdit);
		btDelete = (Button) findViewById(R.id.btClientDelete);

		Intent intent = getIntent();

		final int id = intent.getIntExtra("id", -1);
		String name = intent.getStringExtra("name");
		String email = intent.getStringExtra("email");
		int priceLevel = intent.getIntExtra("priceLevel", -1);
		final int position = intent.getIntExtra("position", -1);

		if (id != -1) {
			client.id = id;
		} else {
			btDelete.setVisibility(View.GONE);
		}
		if (name != null) {
			client.name = name;
			etName.setText(name);
		}
		if (email != null) {
			client.email = email;
			etEmail.setText(email);
		}
		if (priceLevel != -1) {
			client.sellPrice = priceLevel;
			etPriceLevel.setText(String.format(Locale.getDefault(), "%d", priceLevel));
		}

		btDelete.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				String url = MainActivity.URL + URL_DELETE + Integer.toString(id);
				StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
						new Response.Listener<String>() {
							@Override
							public void onResponse(String response) {
								if (position == -1) {
									Toast.makeText(EditClientActivity.this,
											getResources().getString(R.string.error_try_later),
											Toast.LENGTH_SHORT).show();
									Log.e(TAG, "wtf");
									return;
								}
								try {
									JSONObject obj = new JSONObject(response);
									if (obj.getBoolean("success")) {
										MainActivity.dManager.deleteClient(id);
										Intent resultIntent = new Intent();
										resultIntent.putExtra("deleted", true);
										resultIntent.putExtra("position", position);
										setResult(Activity.RESULT_OK, resultIntent);
										finish();
									} else if (obj.getBoolean("error")) {
										Toast.makeText(EditClientActivity.this,
												getResources().getString(R.string.error_try_later),
												Toast.LENGTH_SHORT).show();
									}
								} catch (Exception e) {
									Toast.makeText(EditClientActivity.this,
											getResources().getString(R.string.error_try_later),
											Toast.LENGTH_SHORT).show();
								}
							}
						},
						new Response.ErrorListener() {
							@Override
							public void onErrorResponse(VolleyError error) {
								Log.w(TAG, error.getMessage());
							}
						}){
				};
				RequestQueue requestQueue = Volley.newRequestQueue(EditClientActivity.this);
				requestQueue.add(stringRequest);
			}
		});

		btEdit.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				final String name = etName.getText().toString();
				final String email = etEmail.getText().toString();
				final int priceLevel = Integer.parseInt(etPriceLevel.getText().toString());
				String url = MainActivity.URL + URL_EDIT;
				StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
						new Response.Listener<String>()
						{
							@Override
							public void onResponse(String response)
							{
								try {
									JSONObject obj = new JSONObject(response);
									boolean newClient;
									try {
										newClient = obj.getBoolean("added");
									} catch (Exception e) {
										newClient = false;
									}
									if (obj.getBoolean("success")) {
										if (!newClient) {
											int newId = obj.getInt("id");
											String newName = obj.getString("name");
											String newEmail = obj.getString("email");
											int newPriceLevel = obj.getInt("price_level");
											Client client = new Client(newId, newName, newEmail,
													newPriceLevel);
											MainActivity.dManager.modifyClient(client);
										} else {
											String newName = obj.getString("name");
											String newEmail = obj.getString("email");
											int newPriceLevel = obj.getInt("price_level");
											Client client = new Client(newName, newEmail,
													newPriceLevel);
											MainActivity.dManager.addClient(client);
										}
										Intent resultIntent = new Intent();
										if (newClient) {
											resultIntent.putExtra("added", true);
										} else {
											if (position == -1) {
												Toast.makeText(EditClientActivity.this,
														getResources().getString(R.string.error_try_later),
														Toast.LENGTH_SHORT).show();
												Log.e(TAG, "really");
												return;
											}
											resultIntent.putExtra("edited", true);
											resultIntent.putExtra("position", position);
										}
										setResult(Activity.RESULT_OK, resultIntent);
										finish();
									} else if (obj.getBoolean("error")) {
										Toast.makeText(EditClientActivity.this,
												getResources().getString(R.string.error_try_later),
												Toast.LENGTH_SHORT).show();
									}
								} catch (Exception e) {
									Toast.makeText(EditClientActivity.this,
											getResources().getString(R.string.error_try_later),
											Toast.LENGTH_SHORT).show();
								}
							}
						},
						new Response.ErrorListener()
						{
							@Override
							public void onErrorResponse(VolleyError error) {
								Toast.makeText(EditClientActivity.this,
										getResources().getString(R.string.error_try_later),
										Toast.LENGTH_SHORT).show();
							}
						}){
					@Override
					protected Map<String, String> getParams()
					{
						Map<String,String> params = new HashMap<String, String>();
						if (id != -1) {
							params.put("id", Integer.toString(id));
						}
						params.put("name", name);
						params.put("email", email);
						params.put("price_level", Integer.toString(priceLevel));
						return params;
					}

				};
				RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
				requestQueue.add(stringRequest);
			}
		});
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		finish();
	}
}
