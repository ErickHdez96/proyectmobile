package com.example.erick.proyectomovil;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientsFragment extends Fragment
{
	private static final String TAG = "ClientsFragment";
	private static final String URL = "client/from/";

	ListView lvClients;
	ArrayList<Client> arrayOfClients;
	ClientsAdapter adapter;

	private View mProgressView;

	public ClientsFragment()
	{
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_clients, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
	{
		lvClients = (ListView) getActivity().findViewById(R.id.lvClients);
		arrayOfClients = new ArrayList<>();
		adapter = new ClientsAdapter(getActivity(), arrayOfClients);
		lvClients.setAdapter(adapter);

		lvClients.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				Client client = adapter.getItem(i);
				if (client == null) {
					Toast.makeText(getActivity(),
							getResources().getString(R.string.error_try_later),
							Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent = new Intent(getActivity(), EditClientActivity.class);
				intent.putExtra("id", client.id);
				intent.putExtra("name", client.name);
				intent.putExtra("email", client.email);
				intent.putExtra("priceLevel", client.sellPrice);
				intent.putExtra("position", i);
				startActivityForResult(intent, EditClientActivity.REQUEST_CODE);
			}
		});

		mProgressView = getActivity().findViewById(R.id.pbClients);
		showProgress(true);

		loadCurrentClients();
		loadClients();
		super.onViewCreated(view, savedInstanceState);
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show)
	{
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			lvClients.setVisibility(show ? View.GONE : View.VISIBLE);
			lvClients.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					lvClients.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			lvClients.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	public void loadCurrentClients()
	{
		ArrayList<Client> clients = MainActivity.dManager.getClients();
		for (Client client : clients) {
			arrayOfClients.add(client);
		}
	}

	public void loadClients()
	{
		long id;
		try {
			id = MainActivity.dManager.getLastClientId();
		} catch (Exception e) {
			id = 0;
			Log.e(TAG, e.getMessage());
		}
		String url = MainActivity.URL + URL + Long.toString(id);
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						onClientsLoad(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.w(TAG, error.getMessage());
					}
				}){
		};
		RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
		requestQueue.add(stringRequest);
	}

	private void onClientsLoad(String response)
	{
		try {
			JSONObject obj = new JSONObject(response);
			JSONArray clientsArray = obj.getJSONArray("clients");
			for (int i = 0; i < clientsArray.length(); i++) {
				JSONObject clientObject = clientsArray.getJSONObject(i);
				Client client = new Client(clientObject.getInt("id"),
						clientObject.getString("name"), clientObject.getString("email"),
						clientObject.getInt("sell_price"));
				MainActivity.dManager.addClient(client);
				adapter.add(client);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		showProgress(false);
	}

	public void addClient()
	{
		Intent intent = new Intent(getActivity(), EditClientActivity.class);
		startActivityForResult(intent, EditClientActivity.REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case EditClientActivity.REQUEST_CODE:
				if (data == null) {
					break;
				}
				if (data.getExtras().getBoolean("deleted", false)) {
					int position = data.getExtras().getInt("position", -1);
					if (position != -1) {
						arrayOfClients.remove(position);
						adapter.notifyDataSetChanged();
					} else {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.error_try_later),
								Toast.LENGTH_SHORT).show();
					}
				} else if (data.getExtras().getBoolean("added", false)) {
					loadClients();
					adapter.notifyDataSetChanged();
				} else if (data.getExtras().getBoolean("edited", false)) {
					int position = data.getExtras().getInt("position", -1);
					if (position != -1) {
						Client client = adapter.getItem(position);
						if (client == null) {
							Log.e(TAG, "I surrender");
							break;
						}
						Client newClient = MainActivity.dManager.getClient(client.id);
						client.id = newClient.id;
						client.name = newClient.name;
						client.email = newClient.email;
						client.sellPrice = newClient.sellPrice;
						adapter.notifyDataSetChanged();
					} else {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.error_try_later),
								Toast.LENGTH_SHORT).show();
					}
				}
				break;
			default:
				Log.w(TAG, "wat");
		}
	}
}
