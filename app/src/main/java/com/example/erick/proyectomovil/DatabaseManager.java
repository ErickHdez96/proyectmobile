package com.example.erick.proyectomovil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

class DatabaseManager extends SQLiteOpenHelper
{
	private static final String TAG = "DATABASE MANAGER";

	private final static String DATABASE = "ecommerce";
	private final static int VERSION = 1;

	private final String CLIENTS_TABLE = "clients";
	private final String PRODUCTS_TABLE = "products";
	private final String SALES_TABLE = "sales";
	private final String SALES_DETAILS_TABLE = "sales_details";
	private final String MESSAGES_TABLE = "messages";
	private final String TODOS_TABLE = "todos";

	private final String CLIENTS_ID = "id";
	private final String CLIENTS_NAME = "name";
	private final String CLIENTS_EMAIL = "email";
	private final String CLIENTS_SELLPRICE = "sell_price";

	private final String SALE_ID = "id";
	private final String SALE_CLIENT = "client";
	private final String SALE_DATE = "date";
	private final String SALE_SUBTOTAL = "subtotal";
	private final String SALE_TOTAL = "total";

	private final String SALEDETAIL_ID = "id";
	private final String SALEDETAIL_SALE = "sale";
	private final String SALEDETAIL_PRODUCT = "product";
	private final String SALEDETAIL_QTY = "qty";
	private final String SALEDETAIL_SUBTOTAL = "subtotal";

	private final String PRODUCT_ID = "id";
	private final String PRODUCT_CODE = "code";
	private final String PRODUCT_NAME = "name";
	private final String PRODUCT_PRICE = "price";
	private final String PRODUCT_PRICE1 = "price1";
	private final String PRODUCT_PRICE2 = "price2";
	private final String PRODUCT_PRICE3 = "price3";
	private final String PRODUCT_PRICE4 = "price4";
	private final String PRODUCT_PRICE5 = "price5";
	private final String PRODUCT_PRICE6 = "price6";

	private final String TODO_ID = "id";
	private final String TODO_TEXT = "text";
	private final String TODO_DONE = "done";

	private final String MESSAGE_ID = "id";
	private final String MESSAGE_SUBJECT = "subject";
	private final String MESSAGE_CLIENT = "client";
	private final String MESSAGE_BODY = "body";
	private final String MESSAGE_DATE = "date";
	private final String MESSAGE_READ = "read";


	DatabaseManager(Context context)
	{
		super(context, DATABASE, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		String CREATE_CLIENTS = "CREATE TABLE " + CLIENTS_TABLE + " (" +
				CLIENTS_ID + " INTEGER PRIMARY KEY, " +
				CLIENTS_NAME + " CHAR(100) NOT NULL, " +
				CLIENTS_EMAIL + " CHAR(50) NULL, " +
				CLIENTS_SELLPRICE + " INTEGER DEFAULT 1);";

		String CREATE_SALES = "CREATE TABLE " + SALES_TABLE + " (" +
				SALE_ID + " INTEGER PRIMARY KEY, " +
				SALE_CLIENT + " INTEGER NOT NULL, " +
				SALE_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
				SALE_SUBTOTAL + " INTEGER DEFAULT 0, " +
				SALE_TOTAL + " INTEGER DEFAULT 0);";

		String CREATE_SALES_DETAILS = "CREATE TABLE " + SALES_DETAILS_TABLE + " (" +
				SALEDETAIL_ID + " INTEGER PRIMARY KEY, " +
				SALEDETAIL_SALE + " INTEGER NOT NULL, " +
				SALEDETAIL_PRODUCT + " INTEGER NOT NULL, " +
				SALEDETAIL_QTY + " INTEGER DEFAULT 1, " +
				SALEDETAIL_SUBTOTAL + " INTEGER DEFAULT 0);";

		String CREATE_PRODUCTS = "CREATE TABLE " + PRODUCTS_TABLE + " (" +
				PRODUCT_ID + " INTEGER PRIMARY KEY, " +
				PRODUCT_CODE + " CHAR(20) NOT NULL, " +
				PRODUCT_NAME + " CHAR(50) NOT NULL, " +
				PRODUCT_PRICE + " INTEGER DEFAULT 0, " +
				PRODUCT_PRICE1 + " INTEGER DEFAULT 0, " +
				PRODUCT_PRICE2 + " INTEGER DEFAULT 0, " +
				PRODUCT_PRICE3 + " INTEGER DEFAULT 0, " +
				PRODUCT_PRICE4 + " INTEGER DEFAULT 0, " +
				PRODUCT_PRICE5 + " INTEGER DEFAULT 0, " +
				PRODUCT_PRICE6 + " INTEGER DEFAULT 0);";

		String CREATE_TODOS = "CREATE TABLE " + TODOS_TABLE + " (" +
				TODO_ID + " INTEGER PRIMARY KEY, " +
				TODO_TEXT + " TEXT NOT NULL, " +
				TODO_DONE + " BOOLEAN DEFAULT FALSE);";

		String CREATE_MESSAGES = "CREATE TABLE " + MESSAGES_TABLE + " (" +
				MESSAGE_ID + " INTEGER PRIMARY KEY, " +
				MESSAGE_SUBJECT + " CHAR(100) NOT NULL, " +
				MESSAGE_CLIENT + " INTEGER NOT NULL, " +
				MESSAGE_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
				MESSAGE_READ + " BOOLEAN DEFAULT FALSE, " +
				MESSAGE_BODY + " TEXT NOT NULL);";
		db.execSQL(CREATE_CLIENTS);
		db.execSQL(CREATE_SALES);
		db.execSQL(CREATE_SALES_DETAILS);
		db.execSQL(CREATE_PRODUCTS);
		db.execSQL(CREATE_TODOS);
		db.execSQL(CREATE_MESSAGES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int i, int i1)
	{
		String DELETE_CLIENTS = "DROP TABLE IF EXISTS " + CLIENTS_TABLE;
		String DELETE_SALES = "DROP TABLE IF EXISTS " + SALES_TABLE;
		String DELETE_SALES_DETAILS = "DROP TABLE IF EXISTS " + SALES_DETAILS_TABLE;
		String DELETE_PRODUCTS = "DROP TABLE IF EXISTS " + PRODUCTS_TABLE;
		String DELETE_TODOS = "DROP TABLE IF EXISTS " + TODOS_TABLE;
		String DELETE_MESSAGES = "DROP TABLE IF EXISTS " + MESSAGES_TABLE;
		db.execSQL(DELETE_CLIENTS);
		db.execSQL(DELETE_SALES);
		db.execSQL(DELETE_SALES_DETAILS);
		db.execSQL(DELETE_PRODUCTS);
		db.execSQL(DELETE_TODOS);
		db.execSQL(DELETE_MESSAGES);
		onCreate(db);
	}

	boolean addClient(Client client)
	{
		SQLiteDatabase db = getReadableDatabase();
		if (client.id != -1) {
			String CHECK_CLIENT = "SELECT " + CLIENTS_ID + " FROM " + CLIENTS_TABLE +
					" WHERE " + CLIENTS_ID + " = " + Integer.toString(client.id);
			Cursor c = db.rawQuery(CHECK_CLIENT, null);
			if (c.moveToFirst()) {
				c.close();
				return false;
			}
			c.close();
		}
		db = getWritableDatabase();

		ContentValues values = new ContentValues();
		if (client.id != -1) {
			values.put(CLIENTS_ID, client.id);
		}
		values.put(CLIENTS_NAME, client.name);
		values.put(CLIENTS_EMAIL, client.email);
		values.put(CLIENTS_SELLPRICE, client.sellPrice);

		try {
			db.insert(CLIENTS_TABLE, null, values);
			return true;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	void deleteClient(int clientId)
	{
		String selection = CLIENTS_ID + " = ?";
		String[] selectionArgs = {Integer.toString(clientId)};
		SQLiteDatabase db = getWritableDatabase();
		db.delete(CLIENTS_TABLE, selection, selectionArgs);
	}

	void modifyClient(Client client)
	{
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CLIENTS_NAME, client.name);
		values.put(CLIENTS_EMAIL, client.email);
		values.put(CLIENTS_SELLPRICE, client.sellPrice);

		String selection = CLIENTS_ID + " = ?";
		String[] selectionArgs = { Integer.toString(client.id) };
		db.update(CLIENTS_TABLE, values, selection, selectionArgs);
	}

	long getLastClientId()
	{
		String SQL_LAST_ID = "SELECT " + CLIENTS_ID + " FROM " + CLIENTS_TABLE +
				" ORDER BY " + CLIENTS_ID + " DESC LIMIT 1";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_LAST_ID, null);
		long id = 0;
		if (c.moveToFirst()) {
			id = c.getLong(c.getColumnIndex(CLIENTS_ID));
		}
		c.close();
		return id;
	}

	ArrayList<Client> getClients()
	{
		ArrayList<Client> clients = new ArrayList<>();
		String SQL_CLIENTS = "SELECT * FROM " + CLIENTS_TABLE;
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_CLIENTS, null);
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex(CLIENTS_ID));
				String name = c.getString(c.getColumnIndex(CLIENTS_NAME));
				String email = c.getString(c.getColumnIndex(CLIENTS_EMAIL));
				int sell_price = c.getInt(c.getColumnIndex(CLIENTS_SELLPRICE));
				Client client = new Client(id, name, email, sell_price);
				clients.add(client);
			} while (c.moveToNext());
		}
		c.close();
		return clients;
	}

	public Client getClient(int id)
	{
		String SQL_CLIENTS = "SELECT * FROM " + CLIENTS_TABLE + " WHERE " +
				CLIENTS_ID + " = " + Integer.toString(id);
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_CLIENTS, null);
		Client client;
		if (c.moveToFirst()) {
			String name = c.getString(c.getColumnIndex(CLIENTS_NAME));
			String email = c.getString(c.getColumnIndex(CLIENTS_EMAIL));
			int sell_price = c.getInt(c.getColumnIndex(CLIENTS_SELLPRICE));
			client = new Client(id, name, email, sell_price);
		} else {
			client = new Client(id);
		}
		c.close();
		return client;
	}

	public Client getClientByName(String clientName)
	{
		SQLiteDatabase db = getReadableDatabase();
		String[] projection = {
				CLIENTS_ID,
				CLIENTS_NAME,
				CLIENTS_EMAIL,
				CLIENTS_SELLPRICE
		};

		String selection = CLIENTS_NAME + " = ?";
		String[] selectionArgs = { clientName };

		Cursor c = db.query(
				CLIENTS_TABLE,
				projection,
				selection,
				selectionArgs,
				null,
				null,
				null
		);
		Client client;
		if (c.moveToFirst()) {
			int id = c.getInt(c.getColumnIndex(CLIENTS_ID));
			String email = c.getString(c.getColumnIndex(CLIENTS_EMAIL));
			int sell_price = c.getInt(c.getColumnIndex(CLIENTS_SELLPRICE));
			client = new Client(id, clientName, email, sell_price);
		} else {
			client = new Client(-1);
		}
		c.close();
		return client;
	}

	boolean addMessage(Message message)
	{
		SQLiteDatabase db = getReadableDatabase();
		if (message.id != -1) {
			String CHECK_MESSAGE = "SELECT " + MESSAGE_ID + " FROM " + MESSAGES_TABLE +
					" WHERE " + MESSAGE_ID + " = " + Integer.toString(message.id);
			Cursor c = db.rawQuery(CHECK_MESSAGE, null);
			if (c.moveToFirst()) {
				c.close();
				return false;
			}
			c.close();
		}
		db = getWritableDatabase();

		ContentValues values = new ContentValues();
		if (message.id != -1) {
			values.put(MESSAGE_ID, message.id);
		}
		values.put(MESSAGE_SUBJECT, message.subject);
		values.put(MESSAGE_CLIENT, message.client);
		values.put(MESSAGE_BODY, message.body);
		values.put(MESSAGE_DATE, message.date);
		values.put(MESSAGE_READ, message.read);

		try {
			db.insert(MESSAGES_TABLE, null, values);
			return true;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	long getLastMessageId()
	{
		String SQL_LAST_ID = "SELECT " + MESSAGE_ID + " FROM " + MESSAGES_TABLE +
				" ORDER BY " + MESSAGE_ID + " DESC LIMIT 1";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_LAST_ID, null);
		long id = 0;
		if (c.moveToFirst()) {
			id = c.getLong(c.getColumnIndex(MESSAGE_ID));
		}
		c.close();
		return id;
	}

	ArrayList<Message> getMessages()
	{
		ArrayList<Message> messages = new ArrayList<>();
		String SQL_MESSAGES = "SELECT * FROM " +
				MESSAGES_TABLE +
				" ORDER BY " +
				MESSAGE_ID +
				" DESC";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_MESSAGES, null);
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex(MESSAGE_ID));
				int client = c.getInt(c.getColumnIndex(MESSAGE_CLIENT));
				String clientName = getClient(client).name;
				String subject = c.getString(c.getColumnIndex(MESSAGE_SUBJECT));
				String body = c.getString(c.getColumnIndex(MESSAGE_BODY));
				String date = c.getString(c.getColumnIndex(MESSAGE_DATE));
				boolean read = c.getInt(c.getColumnIndex(MESSAGE_READ)) > 0;
				Message message = new Message(id, subject, client, clientName, body, date, read);
				messages.add(message);
			} while (c.moveToNext());
		}
		c.close();
		return messages;
	}

	long addSale(Sale sale)
	{
		SQLiteDatabase db = getReadableDatabase();
		if (sale.id != -1) {
			String CHECK_SALE = "SELECT " + SALE_ID + " FROM " + SALES_TABLE +
					" WHERE " + SALE_ID + " = " + Integer.toString(sale.id);
			Cursor c = db.rawQuery(CHECK_SALE, null);
			if (c.moveToFirst()) {
				c.close();
				return -1;
			}
			c.close();
		}
		db = getWritableDatabase();

		ContentValues values = new ContentValues();
		if (sale.id != -1) {
			values.put(SALE_ID, sale.id);
		}
		values.put(SALE_CLIENT, sale.client);
		if (sale.date != null) {
			values.put(SALE_DATE, sale.date);
		}
		values.put(SALE_SUBTOTAL, sale.subtotal);
		values.put(SALE_TOTAL, sale.total);

		try {
			return db.insert(SALES_TABLE, null, values);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return -1;
		}
	}

	void deleteSale(int saleId)
	{
		String selection = SALE_ID + " = ?";
		String[] selectionArgs = {Integer.toString(saleId)};
		SQLiteDatabase db = getWritableDatabase();
		db.delete(SALES_TABLE, selection, selectionArgs);
	}

	long addSaleDetail(SaleDetail saleDetail)
	{
		SQLiteDatabase db = getReadableDatabase();
		if (saleDetail.id != -1) {
			String CHECK_SALE = "SELECT " + SALEDETAIL_ID + " FROM " + SALES_DETAILS_TABLE +
					" WHERE " + SALEDETAIL_ID + " = " + Integer.toString(saleDetail.id);
			Cursor c = db.rawQuery(CHECK_SALE, null);
			if (c.moveToFirst()) {
				c.close();
				return -1;
			}
			c.close();
		}
		db = getWritableDatabase();

		ContentValues values = new ContentValues();
		if (saleDetail.id != -1) {
			values.put(SALEDETAIL_ID, saleDetail.id);
		}
		values.put(SALEDETAIL_PRODUCT, saleDetail.product);
		values.put(SALEDETAIL_SALE, saleDetail.sale);
		values.put(SALEDETAIL_QTY, saleDetail.qty);
		values.put(SALEDETAIL_SUBTOTAL, saleDetail.subtotal);

		try {
			return db.insert(SALES_DETAILS_TABLE, null, values);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return -1;
		}
	}

	long getLastSaleId()
	{
		String SQL_LAST_ID = "SELECT " + SALE_ID + " FROM " + SALES_TABLE +
				" ORDER BY " + SALE_ID + " DESC LIMIT 1";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_LAST_ID, null);
		long id = 0;
		if (c.moveToFirst()) {
			id = c.getLong(c.getColumnIndex(SALE_ID));
		}
		c.close();
		return id;
	}

	ArrayList<Sale> getSales()
	{
		ArrayList<Sale> sales = new ArrayList<>();
		String SQL_SALES = "SELECT * FROM " + SALES_TABLE + " ORDER BY " + SALE_ID + " DESC";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_SALES, null);
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex(SALE_ID));
				int client = c.getInt(c.getColumnIndex(SALE_CLIENT));
				String clientName = getClient(client).name;
				String date = c.getString(c.getColumnIndex(SALE_DATE));
				int subtotal = c.getInt(c.getColumnIndex(SALE_SUBTOTAL));
				int total = c.getInt(c.getColumnIndex(SALE_TOTAL));
				Sale sale = new Sale(id, client, clientName, date, subtotal, total);
				sales.add(sale);
			} while (c.moveToNext());
		}
		c.close();
		return sales;
	}

	boolean addProduct(Product product)
	{
		SQLiteDatabase db = getReadableDatabase();
		if (product.id != -1) {
			String CHECK_PRODUCT = "SELECT " + PRODUCT_ID + " FROM " + PRODUCTS_TABLE +
					" WHERE " + PRODUCT_ID + " = " + Integer.toString(product.id);
			Cursor c = db.rawQuery(CHECK_PRODUCT, null);
			if (c.moveToFirst()) {
				c.close();
				return false;
			}
			c.close();
		}
		db = getWritableDatabase();

		ContentValues values = new ContentValues();
		if (product.id != -1) {
			values.put(PRODUCT_ID, product.id);
		}
		values.put(PRODUCT_CODE, product.code);
		values.put(PRODUCT_NAME, product.name);
		values.put(PRODUCT_PRICE, product.price);
		values.put(PRODUCT_PRICE1, product.price1);
		values.put(PRODUCT_PRICE2, product.price2);
		values.put(PRODUCT_PRICE3, product.price3);
		values.put(PRODUCT_PRICE4, product.price4);
		values.put(PRODUCT_PRICE5, product.price5);
		values.put(PRODUCT_PRICE6, product.price6);

		try {
			db.insert(PRODUCTS_TABLE, null, values);
			return true;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	long getLastProductId()
	{
		String SQL_LAST_ID = "SELECT " + PRODUCT_ID + " FROM " + PRODUCTS_TABLE +
				" ORDER BY " + PRODUCT_ID + " DESC LIMIT 1";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_LAST_ID, null);
		long id = 0;
		if (c.moveToFirst()) {
			id = c.getLong(c.getColumnIndex(PRODUCT_ID));
		}
		c.close();
		return id;
	}

	ArrayList<Product> getProducts()
	{
		ArrayList<Product> products = new ArrayList<>();
		String SQL_PRODUCTS = "SELECT * FROM " + PRODUCTS_TABLE + " ORDER BY " +
				PRODUCT_ID +" DESC";
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_PRODUCTS, null);
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex(PRODUCT_ID));
				String code = c.getString(c.getColumnIndex(PRODUCT_CODE));
				String name = c.getString(c.getColumnIndex(PRODUCT_NAME));
				int price = c.getInt(c.getColumnIndex(PRODUCT_PRICE));
				int price1 = c.getInt(c.getColumnIndex(PRODUCT_PRICE1));
				int price2 = c.getInt(c.getColumnIndex(PRODUCT_PRICE2));
				int price3 = c.getInt(c.getColumnIndex(PRODUCT_PRICE3));
				int price4 = c.getInt(c.getColumnIndex(PRODUCT_PRICE4));
				int price5 = c.getInt(c.getColumnIndex(PRODUCT_PRICE5));
				int price6 = c.getInt(c.getColumnIndex(PRODUCT_PRICE6));
				Product product = new Product(id, code, name, price, price1, price2, price3,
						price4, price5, price6);
				products.add(product);
			} while (c.moveToNext());
		}
		c.close();
		return products;
	}

	public Product getProduct(int id)
	{
		String SQL_PRODUCTS = "SELECT * FROM " + PRODUCTS_TABLE + " WHERE " +
				PRODUCT_ID + " = " + Integer.toString(id);
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery(SQL_PRODUCTS, null);
		Product product;
		if (c.moveToFirst()) {
			String code = c.getString(c.getColumnIndex(PRODUCT_CODE));
			String name = c.getString(c.getColumnIndex(PRODUCT_NAME));
			int price = c.getInt(c.getColumnIndex(PRODUCT_PRICE));
			int price1 = c.getInt(c.getColumnIndex(PRODUCT_PRICE1));
			int price2 = c.getInt(c.getColumnIndex(PRODUCT_PRICE2));
			int price3 = c.getInt(c.getColumnIndex(PRODUCT_PRICE3));
			int price4 = c.getInt(c.getColumnIndex(PRODUCT_PRICE4));
			int price5 = c.getInt(c.getColumnIndex(PRODUCT_PRICE5));
			int price6 = c.getInt(c.getColumnIndex(PRODUCT_PRICE6));
			product = new Product(id, code, name, price, price1, price2, price3,
					price4, price5, price6);
		} else {
			product = new Product(id);
		}
		c.close();
		return product;
	}

	public Product getProductByCode(String productCode)
	{
		SQLiteDatabase db = getReadableDatabase();
		String[] projection = {
				PRODUCT_ID,
				PRODUCT_CODE,
				PRODUCT_NAME,
				PRODUCT_PRICE,
				PRODUCT_PRICE1,
				PRODUCT_PRICE2,
				PRODUCT_PRICE3,
				PRODUCT_PRICE4,
				PRODUCT_PRICE5,
				PRODUCT_PRICE6,
		};

		String selection = PRODUCT_CODE + " = ?";
		String[] selectionArgs = { productCode };

		Cursor c = db.query(
				PRODUCTS_TABLE,
				projection,
				selection,
				selectionArgs,
				null,
				null,
				null
		);
		Product product;
		if (c.moveToFirst()) {
			int id = c.getInt(c.getColumnIndex(PRODUCT_ID));
			String code = c.getString(c.getColumnIndex(PRODUCT_CODE));
			String name = c.getString(c.getColumnIndex(PRODUCT_NAME));
			int price = c.getInt(c.getColumnIndex(PRODUCT_PRICE));
			int price1 = c.getInt(c.getColumnIndex(PRODUCT_PRICE1));
			int price2 = c.getInt(c.getColumnIndex(PRODUCT_PRICE2));
			int price3 = c.getInt(c.getColumnIndex(PRODUCT_PRICE3));
			int price4 = c.getInt(c.getColumnIndex(PRODUCT_PRICE4));
			int price5 = c.getInt(c.getColumnIndex(PRODUCT_PRICE5));
			int price6 = c.getInt(c.getColumnIndex(PRODUCT_PRICE6));
			product = new Product(id, code, name, price, price1, price2, price3,
					price4, price5, price6);
		} else {
			product = new Product(-1);
		}
		c.close();
		return product;
	}
}
