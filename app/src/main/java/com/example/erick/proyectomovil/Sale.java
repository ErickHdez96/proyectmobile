package com.example.erick.proyectomovil;

public class Sale
{
	public int id;
	public int client;
	public String clientName;
	public String date;
	public int subtotal;
	public int total;

	public Sale(int id, int client, String clientName, String date, int subtotal, int total)
	{
		this.id = id;
		this.client = client;
		this.clientName = clientName;
		this.date = date;
		this.subtotal = subtotal;
		this.total = total;
	}
}
