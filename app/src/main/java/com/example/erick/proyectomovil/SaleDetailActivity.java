package com.example.erick.proyectomovil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SaleDetailActivity extends AppCompatActivity
{
	private static final String TAG = "SALE DETAIL FRAGMENT";
	private static final String URL = "sale/details/";

	ListView lvDetails;
	ArrayList<SaleDetail> arrayOfSalesDetails;
	SaleDetailAdapter adapter;

	TextView tvClientsName;
	TextView tvDate;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sale_detail);

		tvClientsName = (TextView) findViewById(R.id.tvDetailClientName);
		tvDate = (TextView) findViewById(R.id.tvDetailDate);

		Intent intent = getIntent();
		int saleId = intent.getIntExtra("saleId", -1);
		String clientsName = intent.getStringExtra("clientsName");
		String date = intent.getStringExtra("saleDate");
		if (saleId == -1 || clientsName == null || date == null) {
			finish();
		}

		tvClientsName.setText(clientsName);
		tvDate.setText(date);

		lvDetails = (ListView) findViewById(R.id.lvDetails);
		arrayOfSalesDetails = new ArrayList<>();
		adapter = new SaleDetailAdapter(getApplicationContext(), arrayOfSalesDetails);
		lvDetails.setAdapter(adapter);

		loadSaleDetails(saleId);
	}

	private void loadSaleDetails(int saleId)
	{
		String url = MainActivity.URL + URL + Integer.toString(saleId);
		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						onSaleDetailsLoad(response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.w(TAG, error.getMessage());
					}
				}){
		};
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
		requestQueue.add(stringRequest);
	}

	private void onSaleDetailsLoad(String response)
	{
		try {
			JSONObject obj = new JSONObject(response);
			JSONArray saleDetailsArray = obj.getJSONArray("sale_details");
			for (int i = 0; i < saleDetailsArray.length(); i++) {
				JSONObject saleDetailsObject = saleDetailsArray.getJSONObject(i);
				SaleDetail saleDetail = new SaleDetail(saleDetailsObject.getInt("id"),
						saleDetailsObject.getInt("sale"), saleDetailsObject.getInt("product"),
						saleDetailsObject.getString("productName"),
						saleDetailsObject.getInt("qty"), saleDetailsObject.getInt("subtotal"));
				adapter.add(saleDetail);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}
}
