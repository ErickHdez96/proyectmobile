package com.example.erick.proyectomovil;

public class Message
{
    public int id;
    public String subject;
    public int client;
    public String clientName;
    public String body;
    public String date;
    public boolean read;

    Message(int id, String subject, int client, String clientName, String body, String date, boolean read)
    {
        this.id = id;
        this.subject = subject;
        this.client = client;
        this.clientName = clientName;
        this.body = body;
        this.date = date;
        this.read = read;
    }

    Message(String subject, int client, String clientName, String body, String date)
	{
        this.subject = subject;
        this.client = client;
        this.clientName = clientName;
        this.body = body;
        this.date = date;
        this.read = false;
    }
}
