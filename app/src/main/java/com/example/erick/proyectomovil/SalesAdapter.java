package com.example.erick.proyectomovil;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class SalesAdapter extends ArrayAdapter<Sale>
{
	private static class ViewHolder
	{
		TextView tvClient;
		TextView tvDate;
		TextView tvTotal;
	}

	SalesAdapter(Context context, List<Sale> objects)
	{
		super(context, R.layout.item_sale, objects);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		Sale sale = getItem(position);
		ViewHolder viewHolder; // view lookup cache stored in tag
		if (convertView == null) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.item_sale, parent, false);
			viewHolder.tvClient = (TextView) convertView.findViewById(R.id.tvSaleClientName);
			viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvSaleDate);
			viewHolder.tvTotal = (TextView) convertView.findViewById(R.id.tvSaleTotal);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (sale == null) {
			Log.e("SALE ADAPTER", "Sale null");
			return convertView;
		}
		viewHolder.tvClient.setText(sale.clientName);
		viewHolder.tvDate.setText(sale.date);
		viewHolder.tvTotal.setText(Integer.toString(sale.total / 100) + "." +
				String.format(Locale.getDefault(), "%02d", sale.total % 100));
		return convertView;
	}
}
