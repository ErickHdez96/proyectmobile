package com.example.erick.proyectomovil;

class Client
{
	public int id;
	public String name;
	public String email;
	public int sellPrice;

	Client(int id, String name, String email, int sellPrice)
	{
		this.id = id;
		this.name = name;
		this.email = email;
		this.sellPrice = sellPrice;
	}

	Client(String name, String email)
	{
		this.id = -1;
		this.name = name;
		this.email = email;
		this.sellPrice = 0;
	}

	Client(String name, String email, int priceLevel)
	{
		this.id = -1;
		this.name = name;
		this.email = email;
		this.sellPrice = priceLevel;
	}

	Client(int id)
	{
		this.id = id;
		this.name = "";
		this.email = "";
		this.sellPrice = 0;
	}

	@Override
	public String toString()
	{
		return "Name: " + this.name + "\nEmail: " + this.email;
	}
}
